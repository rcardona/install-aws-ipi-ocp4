**Openshift Container Platform v4.4 IPI Installation On AWS Customized Cluster**

In OpenShift Container Platform version 4.4, you can install a customized cluster on infrastructure that the installation program provisions on Amazon Web Services (AWS). To customize the installation, you modify parameters in the install-config.yaml file before you install the cluster. The installation program does use the baseDomain that you specify to create a private Route 53 Zone and the required records for the cluster. The cluster is configured so that the Operators do not create public records for the cluster and all cluster machines are placed in the private subnets that you specify.

LIMITATIONS
The ability to add public functionality to a private cluster is limited.

You cannot make the Kubernetes API endpoints public after installation without taking additional actions, including creating public subnets in the VPC for each availablity zone in use, creating a public load balancer, and configuring the control plane security groups to allow traffic from Internet on 6443 (Kubernetes API port).

If you use a public Service type load balancer, you must tag a public subnet in each availability zone with kubernetes.io/cluster/<cluster-infra-id>: shared so that AWS can use them to create public load balancers.


**Prerequisites:**

- *Configure an AWS account to host the cluster*.
The AIM user must use long-lived credentials, as the cluster continues to use these credentials to manage aws resources for the entire life of the cluster. The account have to have rights to deploy and manage objects in the following aws services; vpcs, route53, ec2, s3 and Elastic Load Balancers.
- *The VPC should have internet access*.
Access to Quay.io is required to obtain the container images needed to install the Openshift cluster.
- *Installation Server*. A RHEL8 server is used in the this procedure as installation server, so called bastion.
- Install the _oc_ client and ocp installer _openshift-install_ on the bastion server. Download the clients here https://cloud.redhat.com/openshift/install/aws/installer-provisioned
- Dowload the secret credentials for the installation. https://cloud.redhat.com/openshift/install/pull-secret
- Install the awscli on the bastion

**Requirements for using your VPC**

The installation program no longer creates the following components:

- Internet gateways
- NAT gateways
- Subnets
- Route tables
- VPCs
- VPC DHCP options
- VPC endpoints

If you use a custom VPC, you must correctly configure it and its subnets for the installation program and the cluster to use. The installation program cannot subdivide network ranges for the cluster to use, set route tables for the subnets, or set VPC options like DHCP, so you must do so before you install the cluster.

Your VPC must meet the following characteristics:

 - The VPC’s CIDR block must contain the Networking.MachineCIDR range, which is the IP address pool for cluster machines.
 - The VPC must not use the kubernetes.io/cluster/.*: owned tag.
 - You must enable the enableDnsSupport and enableDnsHostnames attributes in your VPC so that the cluster can use the Route53 zones that are attached to the VPC to resolve cluster’s internal DNS records. See DNS Support in Your VPC in the AWS documentation.

If you use a cluster with public access, you must create a public and a private subnet for each availability zone that your cluster uses. The installation program modifies your subnets to add the kubernetes.io/cluster/.*: shared tag, so your subnets must have at least one free tag slot available for it. Review the current Tag Restrictions in the AWS documentation to ensure that the installation program can add a tag to each subnet that you specify.

If you are working in a disconnected environment, you are unable to reach the public IP addresses for EC2 and ELB endpoints. To resolve this, you must create a VPC endpoint and attach it to the subnet that the clusters are using. The endpoints should be named as follows:

 - ec2.<region>.amazonaws.com
 - elasticloadbalancing.<region>.amazonaws.com
 - s3.<region>.amazonaws.com

***STEPS***

*0 - Create ssh keys* : This key will be used to connect to the nodes and perform the installation. It would be also needed in case of troubleshooting.

- On the rhel8 installation server, start the ssh-agent process as a background task

 `$ ssh-keygen -t rsa -b 2048 -N '' -f ~/.ssh/id_rsa`

- Start the ssh-agent process as a background task

 `$ eval "$(ssh-agent -s)"`

- Add your SSH private key to the ssh-agent

 `$ ssh-add ~/.ssh/id_rsa`

*1 - Clone this repo to your installation server*

  `$ git clone https://gitlab.com/rcardona/install-aws-ipi-ocp4.git`

*2 - Create an VPC (ready customized) to host the Openshift Cluster*

- Update the parameters file with the needed values

 `install-aws-ipi-ocp4/templates/2.2.hostedzone/cluster-hosted-zone-parameters.json`

- Create the Public Hosted zone

 `aws cloudformation create-stack --stack-name hostedzone --template-body file://install-aws-ipi-ocp4/templates/2.1.vpc/cluster-vpc-cloudformation-template.yaml --parameters file://install-aws-ipi-ocp4/templates/2.1.vpc/cluster-vpc-parameters.json`

*3 - Create the installation files*

- Create the installation directory

 `$ mkdir ocp-installation`

- Create the _install-config.yaml_ file. Input the necessary values; name of domain (PublicHostedZone) created for the installation, , name of the cluster, select the ssh previously created, input pull secret credentials downloaded from https://cloud.redhat.com, etc.

 `$ openshift-install create install-config --dir ocp-installation --log-level debug`

- Customized the  _install-config.yaml_ file.

 `ocp-installation/install-config.yaml`

 There is a important number of fields that can be updated to customize the installation. Here bellow there are a few from the optional section, but the complete list can be found here https://docs.openshift.com/container-platform/4.4/installing/installing_aws/installing-aws-customizations.html.

  - `compute.platform.aws.rootVolume.iops`
  - `compute.platform.aws.rootVolume.size`
  - `compute.aws.region`
  - `platform.aws.userTags`

*4 - Run the installation*

  `$ openshift-install create cluster --dir ocp-installation --log-level debug`

*5 - Check the cluster health*

- Load the kubernetes environment

 `$ export KUBECONFIG=~/ocp-installation/auth/kubeconfig`

- Query Openshift cluster health

 `$ oc get clusterversion`

- Query the provioned openshift nodes

 `$ oc get nodes`


Now the cluster is ready to serve applications.
END
